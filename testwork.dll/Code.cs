﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection;
using System.Xml;
using System.IO;
using DevExpress.Utils;
using testwork.data;
using System.Drawing;

namespace testwork.code
{

    public class Code
    {
        private static Code instance;
        public List<HeatingPanel> HeatingsPanels { get; }

        private Code()
        {
            this.HeatingsPanels = new List<HeatingPanel>();
        }
        public static Code GetInstance()
        {
            if (instance == null)
                instance = new Code();
            return instance;
        }

        public void ParseXML(string xml)
        {
            HeatingsPanels.Clear();
            XmlReader reader = XmlReader.Create(new StringReader(xml));
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "HeatingPanel":

                                HeatingPanel panel = new HeatingPanel();
                                Parse(panel, reader);
                                HeatingsPanels.Add(panel);

                                break;
                            case "HeatingLine":

                                HeatingLine line = new HeatingLine();
                                Parse(line, reader);
                                FindParentHeatingPanel(line.ParentId).HeatingLines.Add(line);

                                break;
                            case "Sensor":
                                Sensor sensor = new Sensor();
                                Parse(sensor, reader);
                                FindParentHeatingLine(sensor.ParentId).Sensors.Add(sensor);
                                break;

                        }
                        break;
                }
            }
            reader.Close();
        }

        private void Parse(object container, XmlReader reader)
        {
            foreach (PropertyInfo pi in container.GetType().GetProperties())
            {
                try
                {
                    switch (pi.PropertyType.ToString())
                    {
                        case "System.Boolean":
                            pi.SetValue(container, bool.Parse(reader.GetAttribute(pi.Name)));
                            break;
                        case "System.Int32":
                            pi.SetValue(container, int.Parse(reader.GetAttribute(pi.Name)));
                            break;
                        case "DevExpress.Utils.PointFloat":
                            pi.SetValue(container, ParseLocation(reader.GetAttribute(pi.Name)));
                            break;
                        case "System.Single":
                            pi.SetValue(container, float.Parse(reader.GetAttribute(pi.Name)));
                            break;
                        case "System.String":
                            pi.SetValue(container, reader.GetAttribute(pi.Name));
                            break;
                    }
                }
                catch (Exception e)
                {
                    int lineNumber = ((IXmlLineInfo)reader).LineNumber;
                    throw new Exception(string.Format("Неудалось получить значение для '{0}'\nОшибка: {1}\nСтрока: {2}", pi.Name, e.Message, lineNumber));
                }

            }
        }


        private PointFloat ParseLocation(string points)
        {
            string[] cords = points.Split(';');
            float x = float.Parse(cords[0]);
            float y = float.Parse(cords[1]);
            return new PointFloat(x, y);
        }
        private HeatingPanel FindParentHeatingPanel(int ParentId)
        {
            HeatingPanel parent = (from h in HeatingsPanels
                                   where h.Id == ParentId
                                   select h).First();

            return parent;
        }

        private HeatingLine FindParentHeatingLine(int ParentId)
        {
            HeatingLine hl = (from parent in HeatingsPanels
                              where parent.HeatingLines.Any(c => c.Id == ParentId)
                              select parent into p
                              from child in p.HeatingLines
                              where child.Id == ParentId
                              select child).First();
            return hl;

        }

    }
}
