﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using DevExpress.Utils;
namespace testwork.data
{
    public class BaseElement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PointFloat Location { get; set; }
        public float Temperature { get; set; }
    }

    public class BaseChildElement : BaseElement
    {
        public int ParentId { get; set; }
        public string State { get; set; }
    }
    public class HeatingPanel  : BaseElement
    {
        public bool IsInAlarm { get; set; }
        public int IsEntryAutomateOn { get; set; }
        public int IsNetworkOn { get; set; }
        public int IsPowerOn { get; set; }
        public int IsOnUps { get; set; }
        public List<HeatingLine> HeatingLines { get; }

        public HeatingPanel()
        {
            this.HeatingLines =  new List<HeatingLine>();
        }
        
    }
    public class HeatingLine : BaseChildElement
    {
        public List<Sensor> Sensors { get; }
        public HeatingLine()
        {
            this.Sensors =  new List<Sensor>();
        }


    }
    public class Sensor : BaseChildElement
    {

    }
}
