﻿using DevExpress.Diagram.Core;
using DevExpress.XtraDiagram;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using testwork.code;
using testwork.data;
namespace testwork.ui
{
    public partial class Form1 : Form
    {
        private DiagramContainer IsEntryAutomateOn = new DiagramContainer() {Size= new SizeF(200F, 300F) };

        private DiagramShape content = new DiagramShape(BasicShapes.Rectangle, 0, 0, 100, 50);

        private DiagramShape temperature = new DiagramShape(BasicShapes.Rectangle, 0, 100, 100, 50);

        private DiagramShape isInAlarm = new DiagramShape(BasicShapes.Rectangle, 0, 200, 200, 100);

        private DiagramShape IsNetworkOnColor = new DiagramShape(BasicShapes.Ellipse, 100, 0, 10, 10);

        private DiagramShape IsNetworkOnText = new DiagramShape(BasicShapes.Rectangle, 100, 0, 100, 50);

        private DiagramShape IsPowerOnColor = new DiagramShape(BasicShapes.Ellipse, 100, 150, 10, 10);

        private DiagramShape IsPowerOnText = new DiagramShape(BasicShapes.Rectangle, 100, 150, 100, 50);

        private DiagramShape IsOnUpsColor = new DiagramShape(BasicShapes.Ellipse, 100, 50, 10, 10);

        private DiagramShape IsOnUpsText = new DiagramShape(BasicShapes.Rectangle, 100, 50, 100, 50);

        private DiagramShape IsEntryAutomateOnColor = new DiagramShape(BasicShapes.Ellipse, 100, 100, 10, 10);

        private DiagramShape IsEntryAutomateOnText = new DiagramShape(BasicShapes.Rectangle, 100, 100, 100, 50);

        private DiagramShape point = new DiagramShape() { Shape = BasicShapes.RoundCornerRectangle, Size = new SizeF(10F, 10F) };

        private OpenFileDialog openFileDialog1 = new OpenFileDialog() { Filter = "XML Files (.xml)|*.xml", FilterIndex = 1 };

        public Form1()

        {
            
            InitializeComponent();
            isInAlarm.Content = "Авария";
            isInAlarm.Appearance.ForeColor = Color.Black;

            IsEntryAutomateOnText.Content = "ВВ. Автомат.";
            IsEntryAutomateOnText.Appearance.ForeColor = Color.Black;

            IsOnUpsText.Content = "На ИБП";
            IsOnUpsText.Appearance.ForeColor = Color.Black;

            IsPowerOnText.Content = "Питание";
            IsPowerOnText.Appearance.ForeColor = Color.Black;

            IsNetworkOnText.Content = "Связь";
            IsNetworkOnText.Appearance.ForeColor = Color.Black;

            temperature.Appearance.ForeColor = Color.Black;
            content.Appearance.ForeColor = Color.Black;

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            DialogResult dialogResult = openFileDialog1.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {

                Stream fileStream = openFileDialog1.OpenFile();

                StreamReader reader = new StreamReader(fileStream);

                string xml = reader.ReadToEnd();

                reader.Close();

                fileStream.Close();

                try
                {
                    Code.GetInstance().ParseXML(xml);
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                List<HeatingPanel> panels = Code.GetInstance().HeatingsPanels;
                diagram.Items.Clear();
                foreach (HeatingPanel panel in panels)
                {

                    IsEntryAutomateOn.Position = panel.Location;

                    IsEntryAutomateOnColor.Appearance.BackColor = GetColor(panel.IsEntryAutomateOn);
                    IsNetworkOnColor.Appearance.BackColor = GetColor(panel.IsNetworkOn);
                    IsPowerOnColor.Appearance.BackColor = GetColor(panel.IsPowerOn);
                    IsOnUpsColor.Appearance.BackColor = GetColor(panel.IsOnUps);


                    temperature.Content = "Темп. " + panel.Temperature + "°";

                    content.Content = panel.Name;

                    isInAlarm.Appearance.BackColor = panel.IsInAlarm ? Color.Red : Color.Gray;

                    IsEntryAutomateOn.Items.Add(IsPowerOnText);
                    IsEntryAutomateOn.Items.Add(IsPowerOnColor);

                    IsEntryAutomateOn.Items.Add(IsNetworkOnText);
                    IsEntryAutomateOn.Items.Add(IsNetworkOnColor);

                    IsEntryAutomateOn.Items.Add(IsEntryAutomateOnText);
                    IsEntryAutomateOn.Items.Add(IsEntryAutomateOnColor);

                    IsEntryAutomateOn.Items.Add(IsOnUpsText);
                    IsEntryAutomateOn.Items.Add(IsOnUpsColor);
                    IsEntryAutomateOn.Items.Add(temperature);
                    IsEntryAutomateOn.Items.Add(isInAlarm);
                    IsEntryAutomateOn.Items.Add(content);

                    diagram.Items.Add(IsEntryAutomateOn);
                    foreach (HeatingLine line in panel.HeatingLines)

                    {
                        DiagramShape LineView = new DiagramShape(BasicShapes.Rectangle) { Position = line.Location, Size = new SizeF(100F, 75F),Content = line.Name + "\n" + line.Temperature };
                        LineView.Appearance.ForeColor = Color.Black;
                        LineView.Appearance.BackColor = GetColor(line.State);
                        diagram.Items.Add(LineView);

                        foreach (Sensor sensor in line.Sensors)
                        {
                            DiagramShape SensorView = new DiagramShape(BasicShapes.RoundedRectangle) { Position = sensor.Location, Size = new SizeF(100F, 75F), Content = sensor.Name + "\n" + sensor.Temperature };
                            SensorView.Appearance.ForeColor = Color.Black;
                            SensorView.Appearance.BackColor = GetColor(sensor.State);
                            diagram.Items.Add(SensorView);
                        }
                    }

                }

            }
        }
        private Color GetColor(string type)
        {
            switch (type)
            {
                case "GoodOrOff":
                    return Color.Green;

                case "Warning":
                    return Color.Yellow;

                case "Alarm":
                    return Color.Red;
                default:
                    return Color.Wheat;
            }
        }
        private Color GetColor(int i)
        {
            switch (i)
            {
                case -1:
                    return Color.Gray;

                case 0:
                    return Color.Red;

                case 1:
                    return Color.Green;

                default:
                    return Color.White;


            }
        }


    }
}

